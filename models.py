from django.db import models
from django.core.validators import MinLengthValidator

class courses_professional_retraining(models.Model):
    class Meta:
        db_table = 'courses_professional_retraining'

    id = models.BigAutoField(primary_key=True)
    professional_retraining = models.CharField("professional_retraining", max_length=200)

    def __str__(self):
        return f'{self.code} {self.description}'


class courses_advanced_training(models.Model):
    class Meta:
        db_table = 'courses_advanced_training'

    id = models.BigAutoField(primary_key=True)
    advanced_training = models.CharField("advanced_training", max_length=200)

    def __str__(self):
        return f'{self.code} {self.description}'

class Universities(models.Model):
    class Meta:
        db_table = 'Universities'

    id = models.BigAutoField(primary_key=True)
    name = models.CharField("Название", max_length=200)
    full_name = models.CharField("Полное_название", max_length=200)
    short_name = models.CharField("Сокращенное_название", max_length=200)
    adress = models.CharField("Адрес", max_length=200)
    site = models.CharField("Сайт", max_length=200)
    mail = models.CharField("Почта", max_length=200)
    tel_number = models.CharField("Номер_телефона", max_length=200)

    def __str__(self):
        return f'{self.code} {self.description}'

class voenkom(models.Model):
    class Meta:
        db_table = 'voenkom'

    id = models.BigAutoField(primary_key=True)
    name = models.CharField("name", max_length=200)
    tel_number = models.CharField("tel_number", max_length=200)
    adress = models.CharField("adress", max_length=200)

    def __str__(self):
        return f'{self.code} {self.description}'