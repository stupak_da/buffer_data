import os
#mark django settings module as settings.py
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "test_dj.settings")
#instantiate a web sv for django which is a wsgi
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


from test_dj.models import voenkom, Universities, \
    courses_professional_retraining, courses_advanced_training

import csv

def courses(file, func):
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\n')
        line_count = 0
        fl = 0
        for row in csv_reader:
            print(row, line_count)
            if fl == 0:
                print(f'Column names are {", ".join(row)}')
                fl += 1
            else:
                line_count += 1
            if(len(row) == 0):
                    line_count -= 1
                    continue
            if '.' in row[0]:
                if row[0][0:row[0].find('.')].isdigit() == True:
                    line_count -= 1
                    continue
            else:
                if func == 0:
                    prof_retrain = courses_professional_retraining()
                else:
                    prof_retrain = courses_advanced_training()
                prof_retrain.id = line_count
                prof_retrain.professional_retraining = row[0]

courses('professional_retraining.csv', 0)
courses('advanced_training.csv', 1)

with open('UNIVERSITIES.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    fl = 0
    for row in csv_reader:
        print(row, line_count)
        if fl == 0:
            print(row, line_count)
            fl +=1
        else:
            line_count += 1
            univer = Universities()
            univer.id = line_count
            univer.name = row[0]
            univer.full_name = row[1]
            univer.short_name = row[2]
            univer.adress = row[3]
            univer.site = row[4]
            univer.mail = row[5]
            univer.tel_number = row[6]
            univer.save()

with open('voenkomaty.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0
    fl = 0
    for row in csv_reader:
        print(row, line_count)
        if fl == 0:
            print(f'Column names are {", ".join(row)}')
            fl += 1
        else:
            line_count += 1

            voen = voenkom()
            voen.id = line_count
            voen.name = row[0]
            voen.tel_number = row[1]
            voen.adress = row[2]
            voen.save()



all_div = voen.objects.count()
print(all_div)